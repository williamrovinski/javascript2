var $ = function(id) { return document.getElementById(id); };
var tasks = [];

var displayTaskList = function() {
    var list = function() {
	// if there are no tasks in task array, check storage
	if (tasks.length === 0) {
		// get tasks from local storage or empty string if nothing in stprage
		var storage = localStorage.getItem("tasks") || "";
		
		// if not empty, convert to array and store in global tasks variable
		if (storage.length > 0) { tasks = storage.split("|"); }
    }
    // if there are tasks in array, sort and create tasks string
    if (tasks.length > 0) {
	    var capitalized = tasks.sort().map(function(value){
			var first = value.substring(0,1);       // get first letter
	        var remaining = value.substring(1);     // get remaining letters
			return first.toUpperCase() + remaining; // capitalize 
    }
    // display tasks striing and set focus on task text box
    $("task_list").value = capitalized && capitalized.join("\n") ||;
    $("task").focus();
};
var addToTaskList = function () {
    var task = $("task");
    if (task.value === "") {
        alert("Please enter a task.");
    } else {
        // add task to array and local storage 
        tasks.push(task.value);
        localStorage.tasks = tasks.join("|");

        // clear task text box and re-display tasks
        task.value = "";
        displayTaskList();
    }
};
var clearTaskList = function() {	
    tasks.length = 0;
	localStorage.tasks = "";
	$("task_list").value = "";
	displayTaskList();
};
window.onload = function() {
	$("add_task").onclick = addToTaskList;
	$("add_task").onclick = addToTaskList;
	displayTasklist();
}; 
