"use strict";
var $ = function(id) { return document.getElementById(id); };
/* create a variable to hold the reference to the timer; make it
   global so all the functions can access it */

var timer;
var counter = 0;

// create the function that the timer calls
var updateCounter = function() {
	counter++;
	$("counter").firstChild.nodeValue = counter;
};

//create a timer that calls the updateCounter function repeatedly
window.onload = function() {
	timer = setInterval( updateCounter, 1000 );
};

var startUpgrade = function() {
    /* code to start the upgrade goes here */
    $("message").firstChild.nodeValue = "Download starting...";
};	

var cancelUpgrade = function() {
	clearTimeout ( timer );
	$("upgrade").setAttribute("class", "closed");
};

window.onload = function() {
   timer = setTimeout( startUpgrade, 5000 );
   $("cancel").onclick = cancelUpgrade;
};

// the event handler for the click event of each h2 element
var toggle = function() {
    var h2 = this;                    // clicked h2 tag     
    var div = h2.nextElementSibling;  // h2 tag's sibling div tag

    // toggle plus and minus image in h2 elements by adding or removing a class
    if (h2.hasAttribute("class")) { 
        h2.removeAttribute("class");	
    } else { 
        h2.setAttribute("class", "minus"); 
    }

    // toggle div visibility by adding or removing a class
    if (div.hasAttribute("class")) { 
        div.removeAttribute("class");
    } else { 
        div.setAttribute("class", "open"); 
    } 
};

var log = function(e) {
	console.log( e );
}; 

window.onload = function() {
    // get the h2 tags
    var faqs = $("faqs");
    var h2Elements = faqs.getElementsByTagName("h2");
    
    // attach event handler for each h2 tag	    
    for (var i = 0; i < h2Elements.length; i++ ) {
    	var h2 = elements[i];                      //h2 tag
		var a = h2.firstChild;                     //h2 tag's child <a> tag
		//attach h2 click event
		evt.attach(h2, "click", toggle);
		//cancel the default action of the <a> tag
		evt.attach(a, "click", evt.preventDefault);
		//log various events of the <a> tag
		evt.attach(a, "click", log);
		evt.attach(a, "focus", log);
		evt.attach(a, "mouseover", log);
	}
	// set focus on first h2 tag's <a> tag
	elements[0].firstChild.focus();
};



